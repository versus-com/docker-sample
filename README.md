# Docker Sample

- Install docker following this [guide](https://docs.docker.com/)
	- For MAC OS and Windows isntall [Kitematic](https://docs.docker.com/kitematic/) first
	- Make sure you have docker on your system: `docker --version`
- On your terminal, do the following:
```
#!bash
$ docker pull node:4.2.2
$ docker pull redis
$ docker pull nginx
```
- Run **redis** image `docker run -d --name redis -p 6379:6379 redis` (Make sure to stop redis if you have one on your machine)
- Run **node** image
```
#!bash
$ cd ${this-repository}/node
$ docker build -t node-versus .
# Make sure the iamge is created by running docker images. You should see an image with name node-versus
$ docker run -d --name node -p 8080:8080 --link redis:redis node-versus
```

Run `docker ps` and you should have two **containers** running.

One thing you should note is that each container has its own IP address. To get the ip address of the container do `docker inspect --format '{{.NetworkSettings.IPAddress}}' node`

Then on your browswer copy the ipaddress plus the port **8080** e.g. `172.17.0.3:8080`

It should output `This page has been viewed 1 times!`

# Run nginx, redis, node  on Cloud Server

The result should have 

- 1 nginx running as load balancer and proxy server
- 3 nodejs application (all functions are the same)
- 1 redis data storage for storing count

1. Install [docker-compose](http://docs.docker.com/compose/install/)
2. Clone this repository
3. `cd` into this repository
4. Run `docker-compose up -d`
5. Run `docker ps` should result to 5 running containers
6. Go to the IP Address of the host server and it should output `This page has been viewed 1 times!`

I already did this on a CoreOS server. You can visit this IP address: `http://46.101.119.128/`