'use strict'
const PORT = process.env.PORT || 8080;

let http = require('http');

let express = require('express');
let redis = require('redis');

let app = express();

let client = redis.createClient('6379', 'redis');

app.get('/', function (req, res, next) {
  client.incr('counter', function (err, counter) {
    if (err) return next(err);

    return res.send(`This page has been viewed ${counter} times!`);
  });
});

http.createServer(app).listen(PORT, function () {
  console.log(`App started! Listening on port ${PORT}`);
});